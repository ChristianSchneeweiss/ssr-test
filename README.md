# SSR Testing

when are components static and when are they dynamic results are

### Static Layout

|            | none   | forced dynamic | revalidate | both    |
|------------|--------|----------------|------------|---------|
| use client | static | static         | static     | static  |
| no client  | static | dynamic        | static     | dynamic |

### Dynamic Layout

|            | none    | forced dynamic | revalidate | both    |
|------------|---------|----------------|------------|---------|
| use client | dynamic | dynamic        | dynamic    | dynamic |
| no client  | dynamic | dynamic        | dynamic    | dynamic |
