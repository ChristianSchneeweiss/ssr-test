import {ReactNode} from "react";

export const dynamic = "force-dynamic";

const Layout = ({children}: { children: ReactNode }) => {
	return (
		<div>
			Dynamic Layout
			{children}
		</div>
	);
};

export default Layout;
